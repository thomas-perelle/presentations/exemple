# Présentation Reveal.js
## Gitlab-CI + Gitlab Pages


--

# Pour plus d'informations

Et quelques explications : https://kgaut.net/blog/2022/deployer-ses-presentations-sur-gitlab-pages-avec-revealjs-docker-et-gitlab-ci.html

---

# Comment ça marche ?
1. Un projet template qui intègre reveal js dans une image docker : https://gitlab.com/formations-kgaut/template-reveal-js
2. Un sous projet, comme celui là, qui contient les slides et utilise l'image docker précédement crée
3. La CI de ce projet publie la présentation automatiquement à chaque modification sur gitlab page.

--

# Intéret ?
- Rédaction de présentation très rapide
- Historique des modifications
- Fork pour réutiliser / faire une nouvelle version de présentation

---

# Exemples

Voici quelques exemples d'utilisation de `RevealJS`

--

# Intégration de code

Voici un exemple de code :

<pre><code data-trim data-noescape>
terraform {
  required_version = ">= 1.7.5"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "1.44.0"
    }
  }
  backend "http" {}
}
</code></pre>

--

# Intégration de code avec highlight de lignes

<pre><code data-trim data-noescape data-line-numbers="2,4-7">
terraform {
  required_version = ">= 1.7.5"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "1.44.0"
    }
  }
  backend "http" {}
}
</code></pre>

--

# Intégration d'image

Intégration d'une image simple.

<img data-src="https://miro.medium.com/v2/resize:fit:1079/1*tjTUpEi8h53-DMkweX2TDQ.png" />

Possibilité d'ajouter du texte ensuite.

--

# Background et gradient
<!-- .slide: data-background-gradient="linear-gradient(to bottom, #283b95, #17b2c3)" -->

Un texte

--

<h2 class="r-fit-text">FIT TEXT</h2>
<h2 class="r-fit-text">CAN BE USED FOR MULTIPLE HEADLINES</h2>